<?php
// require (dirname(__FILE__) . "/../../helper/login.php");
// if (! loginAuthorized()) {
// return;
// }
// }
require (dirname(__FILE__) . "/../../model/imovel.php");
require (dirname(__FILE__) . "/../../model/cliente.php");
require (dirname(__FILE__) . "/../../model/endereco.php");
require (dirname(__FILE__) . "/../../model/solicitacao.php");

$solicitacoes = array();

$endereco = new Endereco();
$endereco->bairro = "granada";
$endereco->cep = "38400100";
$endereco->cidade = "uberlândia";
$endereco->estado = "minas gerais";
$endereco->numero = "101";
$endereco->rua = "rua do jõao";

$cliente = new Cliente();
$cliente->cpf = "12312312312";
$cliente->email = "test";
$cliente->Endereco = $endereco;
$cliente->estadoCivil = "Solteiro";
$cliente->genero = "Masculino";
$cliente->nome = "testName";
$cliente->profissao = "testProfissao";

$endereco = new Endereco();
$endereco->bairro = "granada";
$endereco->cep = "38400100";
$endereco->cidade = "uberlândia";
$endereco->estado = "minas gerais";
$endereco->numero = "101";
$endereco->rua = "rua do jõao";

$imovel = new Imovel();
$imovel->area = "200";
$imovel->armarioEmbutido = true;
$imovel->Cliente = $cliente;
$imovel->dataConstrucao = "01/10/2018";
$imovel->descricao = "descrição do Imovel";
$imovel->Endereco = $endereco;
$imovel->fotos = array(
    "foto1.png",
    "foto2.png",
    "foto3.png",
    "foto4.png",
    "foto5.png",
    "foto6.png"
);
$imovel->qtdQuartos = 4;
$imovel->qtdSalaEstar = 1;
$imovel->qtdSalaJantar = 1;
$imovel->qtdSuites = 1;
$imovel->qtdVagasGaragem = 1;
$imovel->tipoAnuncio = "Alugar";
$imovel->tipoImovel = "Casa";
$imovel->valorImovel = 1300.25;

$solicitacao = new Solicitacao();
$solicitacao->Cliente = $cliente;
$solicitacao->Imovel = $imovel;
$solicitacao->data = '01/07/2018';

$solicitacoes[] = $solicitacao;

$endereco = new Endereco();
$endereco->bairro = "granada";
$endereco->cep = "38400100";
$endereco->cidade = "uberlândia";
$endereco->estado = "minas gerais";
$endereco->numero = "101";
$endereco->rua = "rua do jõao";

$cliente = new Cliente();
$cliente->cpf = "12312312312";
$cliente->email = "test";
$cliente->Endereco = $endereco;
$cliente->estadoCivil = "Solteiro";
$cliente->genero = "Masculino";
$cliente->nome = "testName";
$cliente->profissao = "testProfissao";

$endereco = new Endereco();
$endereco->bairro = "granada";
$endereco->cep = "38400100";
$endereco->cidade = "uberlândia";
$endereco->estado = "minas gerais";
$endereco->numero = "101";
$endereco->rua = "rua do jõao";

$imovel = new Imovel();
$imovel->area = "200";
$imovel->armarioEmbutido = true;
$imovel->Cliente = $cliente;
$imovel->dataConstrucao = "01/10/2018";
$imovel->descricao = "descrição do Imovel";
$imovel->Endereco = $endereco;
$imovel->fotos = array(
    "foto1.png",
    "foto2.png",
    "foto3.png",
    "foto4.png",
    "foto5.png",
    "foto6.png"
);
$imovel->qtdQuartos = 4;
$imovel->qtdSalaEstar = 1;
$imovel->qtdSalaJantar = 1;
$imovel->qtdSuites = 1;
$imovel->qtdVagasGaragem = 1;
$imovel->tipoAnuncio = "Alugar";
$imovel->tipoImovel = "Casa";
$imovel->valorImovel = 1300.25;

$solicitacao = new Solicitacao();
$solicitacao->Cliente = $cliente;
$solicitacao->Imovel = $imovel;
$solicitacao->data = '01/08/2018';

$solicitacoes[] = $solicitacao;

echo json_encode($solicitacoes);


?>