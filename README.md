# Imobiliaria
Projeto iniciado pelos alunos de PPI em 2018/2.

# EXPLICANDO O PROJETO:
- Todos os arquivos importados devem estar nas pastas: css, js, img, fonts e files;

- Todos os includes do projeto devem ser colocados na pasta include.(sessão/menus/footer..);

- Todas as páginas do sistema admin devem ser colocadas na pasta telas;

- Os icones utilizados no sistema admin são dos seguintes links: <br>
  http://ionicons.com/ <br>
  http://fontawesome.io/icons/ <br>
  https://adminlte.io/themes/AdminLTE/pages/UI/icons.html<br>

- Esse primeiro index será landing page;

- Cores temas usadas no projeto: Lilas(#9D5EB5), Laranja(#FC9A00), Azul(#009E86).

- Fontes usadas no projeto: Corbel, Fredoka One.

# REGRAS PARA O PROJETO:
- Toda vez que forem dar commit, especificar o que foi alterado na mensagem;

- Deve-se deixar COMENTARIOS de fácil compreensão ;

- Nomes de arquivos devem começar com letra minuscula;

- Nomes compostos de arquivos devem conter ( - ) p/ separar;

- Nomes para classes css devem ser camelCase com primeira palavra minusculo e segunda palavra iniciada com letra Maiuscula;

- Sempre que der commit, enviar no TRELLO:
  https://trello.com/b/Zu1mZUl7/trabalho-ppi
